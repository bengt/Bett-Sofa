Bett-Sofa
========

The Bett-Sofa is a bed as well as a sofa.

Design
------

-   We tried to buy a sofa from IKEA but were dissatisfied with the lacking in robustness, size and convertability.
    Turns out, we could create something more fitting to our needs with a fraction of the cost of even a entry level sofa.
-   Everything was designed in a few days in inkscape, a 2D vector editor.
    As the Bett-Sofa is a complex, multilayered and even converting structure.
    It was quite hard to figure everything out and in effect we did not get everthing right.
    Inkscape supports rotating elements, which allowed us to visualize the angles of seating are and backrest, which certainly helped a lot.

Practicability
-------------

-   The Bett-Sofa was thought to provide comfortable seats for up to 4 people.
    As it turns out, it is ok for 4 people, but for longer periods of time one should consider using it for 3 people, only.
-   Due to the soft and backwards bent seating area, it is very cozy, inviting to linger.
    For the same reason it is somewhat inconvenient for eating on a couchtable, though.

Parts List
---------

-   12 x pine planks in 12 cm x 2,4 cm x 3 m
-   36 x M6 Screws + 2 washers + 1 nut
-   a few Spax screws
-   1 x 1 m x 4 cm beach spars
-   1 thick and soft matress in 100 x 220 cm
-   1 thin and firm matress in 90 x 200 cm
-   1 slatted frame in 70 x 200 cm
-   1 slatted frame in 90 x 220 cm
-   cost: ~ 50 € total (matresses and the seating slatted frame already existed)

Construction
-----------

-   We underestimated the complexity by far.
    We assumed two to three afternoons and planned to get everything done on a weekend, but we needed about a week to finish.
-   Construction caused a lot of wood dust, as many parts need to be rounded and sewed.
    We did not use an dust extraction system and were ok with it, but it certainly would have been a good idea.
-   We used a garage as a project of this size requires a roomy workshop.
-   Power tools are also a definitve must if you want to progress speedly.
-   We used prints of the construction found here to which we added the dimensioning extracted

Known Bugs
---------

-   The backrest will not stand on its own as intended.
    Supporting stands were added as a workaround.
-   One front corner broke when someone sat down on it.
    A more solid solution should be considered.
-   The designed stands were replaced by pivoting shelves.
    The documentation was never updated to this.
-   There is an error in reasoning in how the planks line up.
    The solution was to add a layer of planks. This cost some additional wood and effort to rethink while constructing, but did not have further effects.
    The documentation was never updated to this.
